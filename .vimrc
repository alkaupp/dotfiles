set nocompatible              " be iMproved, required
"filetype off                  " required
filetype indent on
filetype plugin on
syntax enable

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Keep Plugin commands between vundle#begin/end.
Plugin 'VundleVim/Vundle.vim'
"Plugin 'Valloric/YouCompleteMe'
Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'vim-scripts/taglist.vim'
Plugin 'lumiliet/vim-twig'
Plugin 'kana/vim-smartinput'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'jamessan/vim-gnupg'

call vundle#end()
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

" -------- UI VISUAL SETTINGS --------
set number
set relativenumber
set tw=79           " width of the document (used by gd)
set nowrap          " don't automatically wrap on load
set fo-=t           " dont automatically wrap text when typing
set colorcolumn=80
set showcmd         " Show (partial) command in status line.
set cursorline      " Highlight current line
set showmatch       " Show matching brackets.
set wildmenu        " Visual autocomplete for command menus
set lazyredraw      " Redraw only when need to
set incsearch       " Incremental search
set hlsearch        " Highlight matches
set background=dark " Dark background bcos why not 
set clipboard=unnamedplus " Yank to system clipboard
"mkdir -p ~/.vim/color && cd ~/.vim/colors
"wget -0 wombat256mod.vim http://www.vimm.org/scripts/download_script.php?scr_id=13400
set t_Co=256
color wombat256mod
" Remember last position when reopening a file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal!  g'\"" | endif
set listchars=eol:$,tab:→\ ,trail:·,extends:>,precedes:<,nbsp:_
highlight ColorColumn ctermbg=223 ctermfg=0
highlight CursorLine ctermbg=238
" ------ SPACES AND TABS ------- 
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab" ------- FOLDING --------
set nofoldenable            " Enable folding
set foldlevelstart=10       " Open most folds
set foldnestmax=10          " 10 nexted fold max????"
set foldmethod=indent       " Other values: marker, manual, expr, syntax, diff
" Fold with <space>
nnoremap <space> za
" ------- MOVEMENT --------
" Move to beginning line with B Move to end of line with E
nnoremap B 0
nnoremap E $
nnoremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
nnoremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h
" ------- RANDOM BINDINGS -------
let mapleader = ","
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
nnoremap <C-n> :nohl<CR>
nnoremap <F3> :set list!<CR>
" Highlight last inserted text with <g><V>
nnoremap gV `[v`]
nnoremap <leader>u :GundoToggle<CR>
nnoremap <leader>t :NERDTreeToggle<CR>
nnoremap <F4> :TlistToggle<CR><C-W>h
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>
vnoremap <Leader>s :sort<CR>
vnoremap < <gv
vnoremap > >gv
" ------ AUTOGROUPS ------
augroup configgroup 
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
    " autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md :call <SID>StripTrailingWhitespaces()
    autocmd FileType java setlocal noexpandtab
    " autocmd FileType java setlocal list
    " autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\-T4 
    autocmd FileType php setlocal expandtab 
    autocmd FileType php setlocal list
    autocmd FileType php setlocal listchars=eol:$,tab:>-
    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
    autocmd FileType php nnoremap <leader>c I//<esc>
    autocmd FileType php vnoremap <leader>c I//<esc>
    autocmd FileType php vnoremap <leader>bc di/*<esc>:pu<CR>o*/<esc>
    autocmd FileType php nnoremap <leader>pd i/*<Return>@param<Return>@return<Return>/<esc><Up>
    autocmd FileType javascript nnoremap <buffer><leader>c I//<esc>
    autocmd FileType javascript vnoremap <buffer><leader>c I//<esc>
    autocmd FileType javascript vnoremap <buffer><leader>bc di/*<esc>:pu<CR>o*/<esc>
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd Filetype php set omnifunc=phpcomplete#CompletePHP
    autocmd Filetype java setlocal omnifunc=javacomplete#Complete
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufEnter *.sh setlocal tabstop=2
    autocmd BufEnter *.sh setlocal shiftwidth=2
    autocmd BufEnter *.sh setlocal softtabstop=2
augroup END

" ------- COPY PASTE ON COMMAND LINE ------
" Escape special characters in a string for exact matching.
" This is useful to copying strings from the file to the search tool
" Based on this -
" http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString (string)
    let string=a:string
    " Escape regex characters
    let string = escape(string, '^$.*\/~[]')
    " Escape the line endings
    let string = substitute(string, '\n', '\\n', 'g')
    return string
endfunction

function! GetVisual() range
    " Save the current register and clipboard
    let reg_save = getreg('"')
    let regtype_save = getregtype('"')
    let cb_save = &clipboard
    set clipboard&

    " Put the current visual selection in the register
    normal! gvy
    let selection = getreg('"')

    " Put tee saved registers and clipboards back
    call setreg('"', reg_save, regtype_save)
    let &clipboard = cb_save

    "Escape any special characters in the selection
    let escaped_selection = EscapeString(selection)

    return escaped_selection
endfunction

" set list listchars=tab:→\ ,trail:·
" Start the find and replace command across the entire file
vmap <leader>z <Esc>:%s/<c-r>=GetVisual()<cr>/
vmap <leader>sw <Esc>:/<c-r>=GetVisual()<cr>/
nnoremap <leader>sw yiw<Esc>/<C-R>"

" When in visual mode,
" before command mode is entered by using :
" Put the contents of any selected text into
" the default register
" leave the text highlighted.
" Based on ierton -
" http://stackoverflow.com/questions/4878980/vim-insert-selected-text-into-command-line
vnoremap : "pygv:
" Map ctrl-P to paste escaped contents of default register in command mode
" Based on bryan kennedy -
" http://stackoverflow.com/questions/676600/vim-search-and-replace-selected-text
" cnoremap <C-P> <c-r>=GetVisual()<cr>

" Mouse & Backspace
set bs=2
" Useful setting
set history=700
set undolevels=700

" Autotags 
set tags+=tags;$HOME
let g:autotagTagsFile="~/.tags"
" Open tag in preview window
nnoremap <C-t>l <Esc>:tabnew<CR>:exe "tjump " . expand("<cword>")<Esc>
nnoremap <silent><C-t> <C-w><C-]><C-w>T
" Setup Pathogen to manage your plugins
" mkdir -p ~/.vim/autoload ~/.vim/bundle
" curl -so ~/.vim/autoload/pathogen.vim https://raw.github.com/

execute pathogen#infect()

" Settings for vim-powerline
set laststatus=2

" Better navigating through omnicomplete option list
set completeopt=longest,menuone
function! OmniPopup(action)
    if pumvisible()
        if a:action == 'j'
            return "\<C-N>"
        elseif a:action == 'k'
            return "\<C-P>"
        endif
    endif
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

" YouCompleteMe
" let g:ycm_key_list_previous_completion=['<Up>']

