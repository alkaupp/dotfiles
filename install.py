#!/usr/bin/env python3

import os

HOME = os.getenv("HOME")
MCKEYMAP = {
        "target": os.path.join(HOME, ".config/mc", "mc.keymap"),
        "source": os.path.abspath("mc.keymap")
        }
VIMRC = {
        "target": os.path.join(HOME, ".vimrc"),
        "source": os.path.abspath(".vimrc")
        }
TMUXCONF = {
        "target": os.path.join(HOME, ".tmux.conf"),
        "source": os.path.abspath(".tmux.conf")
        }
CONFIGURATIONS = [MCKEYMAP, VIMRC, TMUXCONF]

def is_symlink(path):
    return os.path.islink(path)

def exists(path):
    return os.path.exists(path)

def install(configs):
    for cfg in configs:
        if not exists(cfg["target"]):
            print("{file} not found, creating symlink.".format(
                file=os.path.basename(cfg["source"])
                ))
            os.symlink(cfg["source"], cfg["target"])
        else:
            if not is_symlink(cfg["target"]):
                print("{file} found but isn't a symlink. Renaming file to {newfile} and creating symlink.".format(
                    file=os.path.basename(cfg["source"]),
                    newfile=os.path.basename(cfg["source"]) + ".bak"
                    ))
                os.rename(cfg["target"], cfg["target"] + ".bak")
                os.symlink(cfg["source"], cfg["target"])

def main():
    install(CONFIGURATIONS)

if __name__ == '__main__':
    main()
